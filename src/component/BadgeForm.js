import React from 'react'

//escribimos rce y ya automaticamente nos crea esto

class BadgeForm extends React.Component {

    state = {};

    /* handleChange = (e) => {
        // console.log(
        //     { name: e.target.name },
        //     { value: e.target.value }
        // );
        this.setState({
            [e.target.name]: e.target.value
        });
     }*/

    handleClick = (e) => {
        console.log('Button has clicked');
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log();
    }

    render() {
        return (
            <div>
                <h1>New Attendant</h1>

                <form onSubmit={this.handleSubmit}>
                    <div className='form-group'>
                        <label htmlFor=''>First Name:</label>
                        <input onChange={this.props.onChange} className='form-control' type='text' name='firstName' value={this.props.formValues.firstName} placeholder='Ingrese Nombre'/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor=''>Last Name:</label>
                        <input onChange={this.props.onChange} className='form-control' type='text' name='lastName' value={this.props.formValues.lastName} placeholder='Ingrese Apellido'/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor=''>Email:</label>
                        <input onChange={this.props.onChange} className='form-control' type='email' name='email' value={this.props.formValues.email} placeholder='Ingrese Email'/>
                    </div>
                    <div className='form-group'>
                        <label htmlFor=''>Job Title:</label>
                        <input onChange={this.props.onChange} className='form-control' type='text' name='jobTitle' value={this.props.formValues.jobTitle} placeholder='Ingrese Empleo'/>
                    </div>
                    <button onClick={this.handleClick} className='btn btn-success'> Guardar </button>
                </form>
            </div>
        )
    }
}

export default BadgeForm;
