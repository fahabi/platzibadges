import React from 'react';

import './styles/Badge.css';
import confLogo from '../images/badge-header.svg';

class Badge extends React.Component{
    render(){
        return (
            <div className='Badge'>
                <div className='Badge__header'>
                    <img src={confLogo} alt="Logo"/>
                </div>
                <div className='Badge__section-name'>
                    <img className='Badge__avatar' src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Escudo-River-1930.png" alt="Avatar"/>
                    <h1>{this.props.firstName} <br/> {this.props.lastName}</h1>
                </div>
                <div className='Badge__section-info'>
                    <h3>Frontend</h3>
                    <div>@fabiramirez61</div>
                </div>
            </div>
        );
    }
}

export default Badge;